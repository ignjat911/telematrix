from telethon.errors.rpcerrorlist import PeerFloodError, UserPrivacyRestrictedError, FloodError, FloodWaitError
from telethon.tl.types import InputPeerEmpty, InputPeerChannel, InputPeerUser
from telethon.tl.functions.channels import InviteToChannelRequest
from telethon.errors.rpcerrorlist import PhoneNumberBannedError
from telethon.tl.functions.messages import GetDialogsRequest
from telethon.sync import TelegramClient
from setup import format_time
import traceback
import random
import time
import json
import sys
import os

"""
Ader je kopija koda sa https://github.com/th3unkn0n/TeleGram-Scraper
jedine razlike su unosenje podataka iz config.json fajla 
jednostvano citanje podataka i json fajla koje se unosi u promenljive
"""

from utils import Utils

re = "\033[1;31m"
gr = "\033[1;32m"
cy = "\033[1;36m"


def banner():
    print()


index = int()
with open(Utils.config_file, 'r') as config_file:
    config_data = json.load(config_file)
    for i, acc in enumerate(config_data['accs']):
        if not acc['banned']:
            index = i


try:
    api_id = config_data['accs'][index]['api_id']
    api_hash = config_data['accs'][index]['api_hash']
    phone = config_data['accs'][index]['phone']
    client = TelegramClient(os.path.join(
        Utils.sessions_folder, phone), api_id, api_hash)

except KeyError:
    Utils.clear()
    banner()
    sys.exit()

client.connect()
if not client.is_user_authorized():
    client.send_code_request(phone)
    Utils.clear()
    banner()
    code = input(gr+'[+] Enter the code: '+re)
    password = input(gr+'[+] Enter the password: '+re)
    client.sign_in(code, password)

Utils.clear()
banner()
input_file = os.path.join(
    Utils.scrape_locations_folder, "vidikovac_users.json")
users = []
with open(input_file, 'r') as f:
    file_data = json.load(f)
    for usr in file_data["users"]:
        users.append(usr)

chats = []
last_date = None
chunk_size = 200
groups = []

result = client(GetDialogsRequest(
    offset_date=last_date,
    offset_id=0,
    offset_peer=InputPeerEmpty(),
    limit=chunk_size,
    hash=0
))
chats.extend(result.chats)

for chat in chats:
    try:
        if chat.megagroup == True:
            groups.append(chat)
    except:
        continue

i = 0
for group in groups:
    print(gr+'['+cy+str(i)+gr+']'+cy+' - '+group.title)
    i += 1

print(gr+'[+] Choose a group to add members')
g_index = input(gr+"[+] Enter a Number : "+re)
target_group = groups[int(g_index)]

target_group_entity = InputPeerChannel(
    target_group.id, target_group.access_hash)


Utils.debug(str(len(users)))
for index, user in enumerate(users):
    time.sleep(1)
    try:
        uid = user['uid']
        access_hash = user['access_hash']
        user_to_add = InputPeerUser(int(uid), int(access_hash))

        participants = client.get_participants(target_group_entity)
        user_found = any(participant.id ==
                         uid for participant in participants)

        if user_found:
            Utils.warning(
                f"[!] User {uid} already in group! Skipping...")
        else:
            client(InviteToChannelRequest(target_group_entity, [user_to_add]))
            Utils.warning("[~] Waiting for 5-10 seconds...")
            time.sleep(random.randint(5, 10))
    except PeerFloodError as flood:
        Utils.error("Getting Flood Error from telegram!")
        time_to_wait = random.randint(flood.seconds, flood.seconds * 2)
        Utils.warning(
            f"[~] Waiting for {format_time(int(time_to_wait))}...")
        time.sleep(time_to_wait)
    except FloodWaitError as flood:
        Utils.error("Getting Flood Error from telegram!")
        time_to_wait = random.randint(flood.seconds, flood.seconds * 2)
        Utils.warning(
            f"[~] Waiting for {format_time(int(time_to_wait))}...")
        time.sleep(time_to_wait)
    except FloodError as flood:
        Utils.error("Getting Flood Error from telegram!")
        time_to_wait = random.randint(flood.seconds, flood.seconds * 2)
        Utils.warning(
            f"[~] Waiting for {format_time(int(time_to_wait))}...")
        time.sleep(time_to_wait)
    except UserPrivacyRestrictedError:
        Utils.error(
            "[!] The user's privacy settings do not allow you to do this. Skipping.")
    except:
        traceback.print_exc()
        Utils.error("[!] Unexpected Error")
        continue
    else:
        print("Adding {}".format(user['uid']))

Utils.ok("DONE")
