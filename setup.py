from pyrogram import Client
from typing import Any, List

from telethon import TelegramClient
from utils import Utils
from time import sleep
import argparse
import json
import os
import sys


def prompt_user() -> int:
    with open(Utils.config_file, "r") as f:
        config_data = json.load(f)
    users = config_data['accs']
    if len(users) == 0:
        print("No users yet!")
        sys.exit()
    possible: List[int] = []
    for index, user in enumerate(users):
        possible.append(index + 1)
        print(
            f"[{index + 1}] {user['uid']} banned: {user['banned']}")
    while True:
        inp = input("Number: ")
        try:
            index = int(inp)
            pass
        except:
            Utils.error("[!] Invalid input!")
            continue
        else:
            if index in possible:
                user = users[index - 1]
                break
            else:
                continue
    return users.index(user)


def list_acocunts() -> None:
    with open(Utils.config_file, 'r') as f:
        file_data = json.load(f)
        accs: List[object] = file_data['accs']
        for acc in accs:
            print(
                f"{Utils.GREEN}{acc['phone']}{Utils.RESET} del: {Utils.GREEN}{acc['deleted']}{Utils.RESET} scam: {Utils.GREEN}{acc['scam']}{Utils.RESET} fake: {Utils.GREEN}{acc['scam']}{Utils.RESET}")


async def add_acount(api_id: int, api_hash: str, phone: str, password: str) -> None:
    new_acc = await get_user(api_id, api_hash, phone, password)
    with open(Utils.config_file, 'r') as f:
        json_data = json.load(f)
        uids = [acc['uid'] for acc in json_data['accs']]
        if new_acc['uid'] not in uids:
            json_data['accs'].append(new_acc)
        else:
            print("User with that udi is already in config")
    with open(Utils.config_file, 'w') as file:
        json.dump(json_data, file, indent=1)


def remove_account(index: int) -> None:
    with open(Utils.config_file, 'r') as f:
        json_data: List = json.load(f)
        json_data['accs'].pop(index)
    with open(Utils.config_file, 'w') as file:
        json.dump(json_data, file, indent=1)


async def get_user(api_id: int, api_hash: str, phone: str, password: str) -> Any:
    session_path = os.path.join(Utils.sessions_folder, phone)
    client = Client(session_path, api_id, api_hash)
    await client.start()
    me = await client.get_me()
    await client.stop()
    uid: int = me.id
    first_name: str = me.first_name
    last_name: str = me.last_name
    username: str = me.username
    new_acc = {
        "api_id": api_id,
        "api_hash": api_hash,
        "phone": phone.replace("+", "").replace(" ", ""),
        "password": password,
        "uid": uid,
        "banned": False,
        "first_name": first_name,
        "last_name": last_name,
        "username": username,
    }
    return new_acc


def install() -> None:
    # REQUIREMENTS
    os.system("pip3 install -r requirements.txt")
    # GROUPS FILES
    if not os.path.exists(Utils.saved_groups):
        os.makedirs(Utils.saved_groups)
    if not os.path.exists(Utils.scrape_group_folder):
        os.makedirs(Utils.scrape_group_folder)
    # LOCATIONS FILES
    if not os.path.exists(Utils.saved_locations):
        os.makedirs(Utils.saved_locations)
    if not os.path.exists(Utils.scrape_locations_folder):
        os.makedirs(Utils.scrape_locations_folder)
    # SESSION FOLDER
    if not os.path.exists(Utils.sessions_folder):
        os.makedirs(Utils.sessions_folder)
    # CONFIG FILE
    if not os.path.exists(Utils.config_file):
        with open(Utils.config_file, 'w') as config_file:
            config_data = {
                "accs": []
            }
            json.dump(config_data, config_file, indent=1)


async def auth():

    config_file = open(Utils.config_file, 'r')

    accs = []

    for acc in json.load(config_file)['accs']:
        accs.append(acc)

    config_file.close()
    current_acc = None

    for index, acc in enumerate(accs):
        if acc['deleted'] != 1 and acc['scam'] != 1 and acc['fake'] != 1:
            current_acc = acc
            Utils.ok(f"[+] Using User {acc['phone']}!")
            break
        else:
            Utils.error(f"[{index + 1}] User {acc['phone']} not usable!")

    if current_acc == None:
        Utils.error(f"[x] No usable accounts!")

    api_id = current_acc['api_id']
    api_hash = current_acc['api_hash']
    phone = current_acc['phone']

    print(f"Account data for {phone}")
    print(f"api_id - {api_id}")
    print(f"api_hash - {api_hash}")
    session_path = os.path.join(Utils.sessions_folder, phone)
    client = TelegramClient(session_path, api_id, api_hash)

    from telethon.errors.rpcerrorlist import PhoneNumberBannedError
    try:
        await client.start()
    except PhoneNumberBannedError:
        Utils.error(f"Phone number banned")
    else:
        print('client starting')

    try:
        await client.get_me()
    except:
        pass
    else:
        print('complete')

    return client


def get_file_name(src_path: str) -> str:
    """
    POMOCNA FUNKCIJA KOJOM OD KORISNIKA DOBIJAMO IME FAJLA IZ ZADATOG FOLDERA    
    """

    # UZIMAMO SVE FAJLOVE IZ src_path
    files = os.listdir(src_path)

    # AKO NEMA TAKVIH GRUPA TO I ISPISUJEMO MO
    if len(files) == 0:
        print("No groups yet!")
        sys.exit()

    file_name: str = ""

    # ITERIRAMO KROZ SVE FAJLOVE SKREJPOVANIH GRUPA PRATECI INDEKS
    # DODAJEMO TAJ INDEKS U LISTU MOGUCIH INDEKSA
    # ISPISUJEMO INDEKS GRUPE I IME GRUPE
    possible: List[int] = []
    for index, group in enumerate(files):
        possible.append(index + 1)
        print(f"[{index + 1}] {group}")

    # PROMPTUJEMO KORISNIKA ZA BROJ SVE DOK TAJ BROJ NIJE VALIDAN
    while True:
        inp = input("Number: ")
        try:
            index = int(inp)
            pass
        except:
            Utils.error("[!] Invalid input!")
            continue
        else:
            if index in possible:
                file_name = files[index - 1]
                break
            else:
                continue

    # VRACAMO IME IZABRANOG FAJLA
    return file_name

# Print iterations progress


def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 *
                                                     (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


def format_time(seconds):
    days = seconds // (24 * 3600)
    hours = (seconds % (24 * 3600)) // 3600
    minutes = (seconds % 3600) // 60
    seconds = seconds % 60

    time_parts = []
    if days:
        time_parts.append(f"{days}d")
    if hours:
        time_parts.append(f"{hours}h")
    if minutes:
        time_parts.append(f"{minutes}m")
    if seconds:
        time_parts.append(f"{seconds}s")

    return ' '.join(time_parts)
