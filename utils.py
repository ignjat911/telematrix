from pyrogram.errors import FloodWait
from typing import Any, Final, List
from time import sleep
import pyfiglet
import json
import time
import sys
import os


class Utils:
    def __init__(self):
        pass

    filters = [
        'О', 'Е', 'А', 'И', 'Н', 'Т', 'С', 'Л', 'В', 'Р', 'К', 'М', 'Д', 'П', 'Ы', 'У', 'Б', 'Я',
        'E', 'T', 'A', 'O', 'I', 'N', 'S', 'H', 'R',
        'Ь', 'Г', 'З', 'Ч', 'Й', 'Ж', 'Х',
        'D', 'L',
        'Ш',
        'C',
        'Ю',
        'U',
        'Ц',
        'M',
        'Э',
        'F',
        'Щ',
        'P',
        'Ф',
        'Ё',
        'W',
        'G',
        'Y',
        # English only characters
        # Russian only characters
        # Ъ
        # Least frequent characters
        # Q, J, X, Z
    ]

    RED: Final = "\u001b[31;1m"
    GREEN: Final = "\u001b[32m"
    RESET: Final = "\033[0;0m"
    YELLOW: Final = "\u001b[33;1m"
    CYAN: Final = "\033[96m"

    SCRAPER_VER: Final = "0.2.1"
    ADDER_VER: Final = "0.2.1"
    GEO_VER: Final = "0.1.3"
    SMS_VER: Final = "0.0.0"

    config_file = "data\\config.json"
    sessions_folder = "data\\sessions"
    requirements_file = "requirements.txt"

    saved_locations = "data\\locations\\saved_locations\\"
    saved_groups = "data\\groups\\saved_groups"

    scrape_locations_folder = "data\\locations\\users"
    scrape_group_folder = "data\\groups\\users"

    def format_seconds(self, sec: int) -> str:
        minutes, seconds = divmod(sec, 60)
        hours, minutes = divmod(minutes, 60)
        formatted_time = f"{hours:02d}:{minutes:02d}:{seconds:02d}"
        return formatted_time

    def format_phone(self, phone: str) -> str:
        temp = phone.replace(" ", "").replace("+", "")
        formated_phone = "+1 {} {} {}".format(
            temp[1:4],
            temp[4:7],
            temp[7:]
        )
        return formated_phone

    def flood_wait(self, e: FloodWait, t: str) -> None:
        wait_time: int = e.value
        for i in range(wait_time + 5, 0, -1):
            Utils.title(t)
            Utils.warning(
                f"[!] Peer Flood waiting for {i} seconds...")
            time.sleep(1)

    def get_profile_photo() -> str:
        profile_photo_dir = "profile_photo"

        for photo_file_path in os.listdir(profile_photo_dir):
            return photo_file_path

    def progress_bar(self, iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
        """
        Call in a loop to create terminal progress bar
        @params:
            iteration   - Required  : current iteration (Int)
            total       - Required  : total iterations (Int)
            prefix      - Optional  : prefix string (Str)
            suffix      - Optional  : suffix string (Str)
            decimals    - Optional  : positive number of decimals in percent complete (Int)
            length      - Optional  : character length of bar (Int)
            fill        - Optional  : bar fill character (Str)
            printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
        """
        percent = ("{0:." + str(decimals) + "f}").format(100 *
                                                         (iteration / float(total)))
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=printEnd)
        # Print New Line on Complete
        if iteration == total:
            print()

    if os.name == "posix":
        def clear() -> None:  os.system("clear")
    elif os.name == "nt":
        def clear() -> None:  os.system("cls")

    def versions():
        sleep(2)
        Utils.color_print(
            f"[+] MATRIX ADDER VER. {Utils.ADDER_VER} loaded!", Utils.GREEN, Utils.RESET)
        print()
        sleep(2)
        Utils.color_print(
            f"[+] MATRIX SCRAPER VER. {Utils.SCRAPER_VER} loaded!", Utils.GREEN, Utils.RESET)
        print()
        sleep(2)
        Utils.color_print(
            f"[+] MATRIX GEO VER. {Utils.GEO_VER} loaded!", Utils.GREEN, Utils.RESET)
        print()

    def title(t: str) -> None:
        Utils.clear()
        f = pyfiglet.Figlet(font='slant')
        Utils.color_print(f.renderText(" ".join(t)),
                          Utils.RESET, Utils.RESET)

    def color_text(text: str, beginning: str, end: str) -> str:
        return beginning + text + end

    def color_print(text: str, beginning: str, end: str) -> None:
        print(Utils.color_text(text, end, beginning))

    def error_text(text: str) -> str:
        return Utils.color_text(text, Utils.RED, Utils.RESET)

    def debug_text(text: str) -> str:
        return Utils.color_text(text, Utils.CYAN, Utils.RESET)

    def warning_text(text: str) -> str:
        return Utils.color_text(text, Utils.YELLOW, Utils.RESET)

    def ok_text(text: str) -> str:
        return Utils.color_text(text, Utils.GREEN, Utils.RESET)

    def reg_text(text: str) -> str:
        return Utils.color_text(text, Utils.RESET, Utils.RESET)

    def error(text: str):
        print(Utils.error_text(text))

    def debug(text: str):
        print(Utils.debug_text(text))

    def warning(text: str):
        print(Utils.warning_text(text))

    def ok(text: str):
        print(Utils.ok_text(text))

    def reg(text: str):
        print(Utils.reg_text(text))
