from telethon.tl.functions.contacts import GetLocatedRequest
from telethon.tl.functions.users import GetFullUserRequest
from telethon import TelegramClient
from setup import auth, printProgressBar
from utils import Utils
from typing import List
import telethon
import json
import sys
import os


async def scrape_location(latitude: float, longitude: float, file_name: str):
    """
    POMOCU LA. I LO. DOBIJMAO GEOLOK. TACKU SA KOJE UZIMAMO KORISNIKE
    LATITUDU I LONGITUDU DOBIJAMO OD KORISNIKA KAO I IME FAJLA U KOJI CE MO SKREJPOVATI KORISNIKE
    """

    # AUTROZACIJA AKUNTA I DOBIJANJE KLAJENT VARIJABLE
    client = await auth()

    # DOBIJANJE GEOLOKACIJSKE TACKE POMOCU LA. I LO. PUTEM KLAJENTA
    point0 = await client(telethon.functions.contacts.GetLocatedRequest(
        geo_point=telethon.types.InputGeoPoint(lat=latitude, long=longitude)))

    # IZ OBJEKTA TACKE KOJI DOBIJAMO UZIMAMO NAJSKORIJI APDEJT I UZIMAMO KORISNIKE KOJI SU U POSLEDNJEM APDEJTU
    users = point0.updates[0].peers

    # KREIRAMO PROMENJLIVU KOJA PREDSTAVLJA PUT FAJLA U KOJI CE KORISNICI BITI UPISANI
    file_path = os.path.join(
        Utils.scrape_locations_folder, f"{file_name}_users.json")

    # PROVERAVAMO DA LI TAJ PUT POSTOJI
    if not os.path.exists(file_path):
        # AKO NE POSTOJI KREIRAMO GA I PRAVIMO TEMPLEJT FAJL GDE CE MO UPISATI KORISNIKE
        users_file = open(file_path, 'w')
        users_data = {
            "users": []
        }
        json.dump(users_data, users_file, indent=1)
    else:
        # AKO POSTOJI FOLDER/FAJL CITAMO GA I STAVLJAMO U PROMENLJIVU RADI KASNIJEG KORISCENJA
        users_file = open(file_path, 'r')
        users_data = json.load(users_file)
        users_file.close()

    Utils.clear()
    # ITERIRAMO KROZ SVE KORISNIKE KOJE SMO PRONASLI NA TOJ LOKACIJI
    for index, user in enumerate(users):
        try:
            # POMOCU KLAJENTA SERVERU TRAZIMO SVE PODATKE O KORISNIKU
            full_user = await client(GetFullUserRequest(user.peer.user_id))
        except:
            continue
        else:
            # OTVARAMO FAJL SA KORISNICIMA
            f = open(file_path, 'r')
            d = json.load(f)
            users_l = len(d['users'])
            users_uids: List = []
            printProgressBar(index + 1, len(users), "Scraping: ",
                             fill="#", suffix=f"{index}/{len(users)}")
            # U KOLIKO SE U FAJLU NALAZE VEC NEKI KORISNICI UZIMAMO NJIHOVE ID-EVE KAKO IH SLUCAJNO NE BI PONOVO DODALI
            if users_l > 0:
                users_uids = [usr['uid'] for usr in d['users']]
            f.close()

            # PROVERAVAMO DA LI SE ID TRENUTNOG KORISNIKA VEC NALAZI U FAJLU
            if full_user.user.id not in users_uids:
                # KREIRAMO NOVI OBJEKAT SA KORISNIKOVIM ID-EM
                new_user = {
                    "uid": full_user.user.id,
                    "access_hash": full_user.user.access_hash
                }
                # U LISTU KORINIKA DODAJEMO NOVOG KORISNIKA
                f = open(file_path, "w")
                d['users'].append(new_user)
                json.dump(d, f, indent=1)
                f.close()

    # IZ NOVO UPISANOG FAJLA UZIMAMO BROJ KORISNIKA RADI DEBAGOVANJA
    f = open(file_path, "r")
    l = len(json.load(f)['users'])
    f.close()
    Utils.clear()
    print(
        f"{Utils.GREEN}[+]{Utils.RESET} All {Utils.GREEN}{l}{Utils.RESET} scraped to {Utils.GREEN}{file_name}_users.json{Utils.RESET}!")


def count_users_in_locations():
    """
    POMOCNA FUNK. KOJA ISPISUJE KOLIKO KORISNIKA JE SKREPOVANO U ODRECENJI LOKACIONI FAJL
    """

    # UZIMAMO IMENA SVIH FAJLOVA SKREJPOVANIH LOKACIJA
    locations: List[str] = os.listdir(Utils.scrape_locations_folder)

    # AKO NEMA NIKAKVIH FAJLOVA TO I ISPISUJEMO
    if len(locations) == 0:
        print("No scraped locations yet!")
        sys.exit()

    # ITERIRAMO KROZ LISTU FAJLOVA PRATECI INDEKS
    for index, location in enumerate(locations):

        # KREIRAMO PUT DO TRENUTNOG LOKACIONOG FAJLA
        location_users_path = os.path.join(
            Utils.scrape_locations_folder, location)

        # OTVARAMO TAJ FAJL, CITAMO PODATKE I ISPISUJEMO BROJ KORISNIKA SKREJPOVANIH U TAJ FAJL
        locations_users_file = open(location_users_path, "r")
        location_users_data = json.load(locations_users_file)
        users_number_text = f"{len(location_users_data['users'])} users"
        print(
            f"[{index + 1}] {location} \t ({users_number_text})")


def list_locations():
    """
    ISPISUJE SVE SACUVANE LOKACIJE U saved_locations FOLDERU
    """

    # U LISTU STAVLJAMO IMENA SVIH SACUVANIH LOKACIONIH FOLDER
    locations: List[str] = os.listdir(Utils.saved_locations)

    # AKO NEMA NIKAKVIH LOKACIJA U FOLDERU SAMO TO I ISPISUJEMO
    if len(locations) == 0:
        print("No locations yet!")
        sys.exit()

    # ITERIRAMO KROZ locations LISTU PRATECI INDEKS ELEMNTA
    for index, location in enumerate(locations):

        # KREIRAMO PUT TO FAJLA
        location_path = os.path.join(
            Utils.saved_locations, location)

        # OTVARAMO TAJ LOKACIONI FAJL I CITAMO PODATKE IZ NJEGA
        location_file = open(location_path, 'r')
        location_data = json.load(location_file)

        # IZPISUJEMO TE PODATKE I ZATVARAMO TAJ FAJL
        print(
            f"[{index + 1}] {location} \t la: {location_data['latitude']} \t lo: {location_data['longitude']}")
        location_file.close()


def add_location(name: str, la: float, lo: float):
    """
    POMOCU ARGUMENTA, IMENA, LATITTUDE i LONGITUDE UNUTAR saved_locations FOLDER
    PRAVIMO NOVI FAJL KOJI CE SADRZATI ZADATE PODATKE (TZV. LOKACIONI FAJL)
    """

    # SVA VELIKA SLOVA PRETVARAMO U MALA I ZAMENJUJEMO RAZMAKE DONJIM CRTAMA
    name = name.lower().replace(" ", "_")

    # KREIRAMO PUTANJU NOVOG FAJLA
    new_loc_path = os.path.join(
        Utils.saved_locations, f"{name}.json")

    # OTVARAMO/KREIRAMO FAJL NA OVOJ PUTANJI
    new_loc_file = open(new_loc_path, 'w')

    # KREIRAMO OBJEKAT KOJI CE BITI UPISAN UNUTAR FAJLA
    new_location = {
        "title": name,
        "latitude": float(la),
        "longitude": float(lo),
    }

    # UPISUJEMO GORE NAVEDENIH OBJEKAT U FAJL I ZATVARAMO FAJL
    json.dump(new_location, new_loc_file, indent=1)
    new_loc_file.close()


def remove_location(file_name: str):
    """
    OD ZADATOG ARGUMENTA, PRONALAZIMO TAJ FAJL I SKLANJAMO GA
    """

    # KREIRAMO PUT DO FAJLA POMOCU ARGUMENTA
    file_path = os.path.join(Utils.saved_locations, file_name)

    # BRISEMO FAJL NA ZADATOJ PUTANJI
    os.remove(file_path)
