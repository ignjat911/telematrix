from utils import Utils
from time import sleep
from scrape import *
from setup import *
from geo import *
import asyncio
import json
import os


async def main():
    Utils.title("MATRIX")

    Utils.ok("[~] ACCOUNTS ")
    # UNOSENJEM PODATAK AKAUNTA AKAUNT SE DODAJE U config.json
    Utils.ok(f"[01] Add account")
    # IZABERES AKAUNTA IZ config.json-a I PROGRAM GA IZBRISE
    Utils.ok(f"[02] Remove account")
    Utils.ok(f"[03] List accounts")  # SAMO ISPISUJEMO AKAUNTOVI U config.json

    Utils.ok("\n[~] GEO ")
    # SKREJPUJE KORISNIKE SA LOKACIJE U FAJL SA IMENOM LOKACIJE
    Utils.ok(f"[04] Scrape from location")
    # DODAVANJE NOVE LOKACIJE SA GEO. SIRINOM I DUZINOM
    Utils.ok(f"[05] Add new location")
    # SKLANJANJE IZABRANJE LOKACIJE IZ FOLDERA SACUVANIH LOKACIJA
    Utils.ok(f"[06] Remove location")
    Utils.ok(f"[07] List locations")  # ISPISIVANJE SVI SACUVANIH LOKACIJA
    # ISPISIVANJE BROJA KORISNIKA IZ FAJLOVA SACUVANIH LOKACIJA
    Utils.ok(f"[08] Count users in locations")

    Utils.ok("\n[~] ADD ")
    Utils.ok(
        f"[09] Add from scraped group file - (users from a scraped group)")
    Utils.ok(
        f"[10] Add from scraped location file - (users from a scraped location)")
    Utils.ok(f"[11] ")

    Utils.ok("\n[~] SCRAPE ")
    # SKREJPOVANJE IZ SACUVANJE GRUPE U ISTOIMENI FAJL
    Utils.ok(f"[12] Scrape to file")
    Utils.ok(f"[13] Add new scraping group")  # DODAVANJA GRUPE ZA SKREJP
    Utils.ok(f"[14] Remove scraping group")  # SKLANJANJE GRUPE ZA SKREJPOVANJE
    # ISPISIVANJE SVIH SKREJPOVANIH FAJLOVA
    Utils.ok(f"[15] List all scraping groups")
    # ISPISIVANJE BROJA KORISNIKA IZ FAJLOVA SACUVANIH GRUPA
    Utils.ok(f"[16] Count users in scraped files")

    Utils.ok("\n[~] OTHER ")
    # INSTALIRANJE BIBILOTEKA, I INICIJALIZACIJA POTREBNIH FOLDERA I FAJLOVA
    Utils.ok(f"[17] Install/Update dependencies, setup project")
    Utils.ok(f"[18] Quit")

    print()
    while True:
        inp = input(Utils.ok_text("Number: "))
        if inp == "1" or inp == "01":
            print("api_id api_hash phone password")
            inp = input("Input: ")
            inp = inp.split(' ')
            api_id = inp[0]
            api_hash = inp[1]
            phone = inp[2]
            password = inp[3]
            await add_acount(api_id, api_hash, phone, password)
            break
        elif inp == "2" or inp == "02":
            index = prompt_user()
            remove_account(index)
            break
        elif inp == "3" or inp == "03":
            list_acocunts()
            break
        elif inp == "4" or inp == "04":
            file_name = get_file_name(Utils.saved_locations)
            file_path = os.path.join(Utils.saved_locations, file_name)
            location_file = open(file_path, 'r')
            location_data = json.load(location_file)
            latitude = location_data['latitude']
            longitude = location_data['longitude']
            title = location_data['title']
            await scrape_location(latitude, longitude, title)
            break
        elif inp == "5" or inp == "05":

            name = input("Name: ")
            latitude = input("Latitude: ")
            longitude = input("Longitude: ")
            add_location(name, latitude, longitude)
            break
        elif inp == "6" or inp == "06":
            file_name = get_file_name(Utils.saved_locations)
            remove_location(file_name)
            break
        elif inp == "7" or inp == "07":
            list_locations()
            break
        elif inp == "8" or inp == "08":
            count_users_in_locations()
            break
        elif inp == "9" or inp == "09":
            # file_name = get_file_name(Utils.saved_groups)
            # await groups_file_to_group(file_name)
            break
        elif inp == "10":
            # file_name = get_file_name(Utils.saved_locations)
            # await locations_file_to_group(file_name)
            break
        elif inp == "11":
            break
        elif inp == "12":
            file_name = get_file_name(Utils.saved_groups)
            file_path = os.path.join(Utils.saved_groups, file_name)
            group_file = open(file_path, 'r')
            group_data = json.load(group_file)
            link = group_data['link']
            title = group_data['title']
            await scrape_group(link, title)
            break
        elif inp == "13":
            group_link = input("Group link: ")
            add_group(group_link)
            break
        elif inp == "14":
            file_name = get_file_name(Utils.saved_groups)
            remove_group(file_name)
            break
        elif inp == "15":
            list_groups()
            break
        elif inp == "16":
            count_users_in_scraped_groups()
            break
        elif inp == "18":
            install()
            break
        elif inp == "19":
            Utils.clear()
            break
        else:
            Utils.error("[!] Invalid input!")
            continue


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
