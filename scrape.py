from telethon.tl.functions.channels import GetFullChannelRequest
from typing import Any, List
from telegram import User
from setup import auth
from utils import Utils
from time import sleep
import json
import sys
import os


def add_group(link: str):
    """
    POMOCU ZADATOG LINKA U FOLDER saved_groups DODAJEMO PODATKE GRUPE U NJEM FAJL
    """

    # MENJAMO IME I LINK KAKO BI BILI U KOREKTNIM FORMATIMA
    name = link.lower().replace("https://t.me/", "")
    link = f"https://t.me/{name}"

    # DOBIJAMO PUT DO FAJLA
    new_group_path = os.path.join(
        Utils.saved_groups, f"{name}.json")

    # OVRAMO ZADATI FAJL I U NJEGA UPISUJEMO PODATKE GRUPE I ZATVARAMO GA
    new_group_file = open(new_group_path, 'w')
    new_location = {
        "title": name,
        "link": link,
    }
    json.dump(new_location, new_group_file, indent=1)
    new_group_file.close()


def remove_group(file_name: str):
    """
    POMOCU IMENA FAJLA SKLANJAMO FAJL IZ FOLDER saved_groups
    """

    # DOBIJAMO PUT DO FAJLA I SKLANJAMO GA IZ FOLDERA
    file_path = os.path.join(Utils.saved_groups, file_name)
    os.remove(file_path)


def list_groups():
    """
    ISPISUJEMO IMENA SVIH GRUPA UNUTAR saved_groups FOLDERA 
    """

    # IZ FOLDERA UZIMAMO IMENA SVIH FAJLOVA
    groups: List[str] = os.listdir(Utils.saved_groups)

    # AKO NEMA FAJLOVA U FOLDERU TO I ISPISUJEMO
    if len(groups) == 0:
        print("No groups yet!")
        sys.exit()

    # ITERIRAMO KROZ LISTU GRUPA PRATECI INDEKS GRUPE
    for index, group in enumerate(groups):
        # ISPISUJEMO INDEKS I IME GRUPE
        print(
            f"[{index + 1}] {group} \t")


def count_users_in_scraped_groups():
    """
    ISPISUJEMO IMENA SVIH SKREJPOVANIH GRUPA KAO I BROJ KORISNIKA UNUTAR NJIH UNUTAR saved_groups FOLDERA 
    """

    # IZ FOLDERA UZIMAMO IMENA SVIH FAJLOVA
    groups: List[str] = os.listdir(Utils.scrape_group_folder)

    # AKO NEMA FAJLOVA U FOLDERU TO I ISPISUJEMO
    if len(groups) == 0:
        print("No scraped groups yet!")
        sys.exit()

    # ITERIRAMO KROZ LISTU GRUPA PRATECI INDEKS GRUPE
    for index, group in enumerate(groups):
        # KREIRAMO PUT DO GRUPE, OTVARAMO JE, I CITAMO PODATKE IZ NJE
        group_path = os.path.join(
            Utils.scrape_group_folder, group)
        group_users_file = open(group_path, "r")
        group_users_data = json.load(group_users_file)
        users_number_text = f"{len(group_users_data['users'])} users"

        # ISPISUJEMO INDEKS, IME GRUPE, I BROJ USERA U NJOJ
        print(
            f"[{index + 1}] {group} \t ({users_number_text})")


async def write_users_to_file(group_members: List, file_name: str) -> None:
    """
    FUNKCIJA KOJA ZADATU LISTU KORISNIKA UPISUJE U FAJL ZA ZADATIM IMENOM
    """

    # KREIRAMO PUTANJU FAJLA
    file_path = os.path.join(
        Utils.scrape_group_folder, f"{file_name}_users.json")

    # PROVERAVAMO DA LI POSTOJI FOLDER/FAJL NA TOJ PUTANJI
    if not os.path.exists(file_path):
        # AKO NE POSTOJI KREIRAMO GA I U NJEGA PISEMO TEMPLEJT PODATKE
        users_file = open(file_path, 'w')
        users_data = {
            "users": []
        }
        json.dump(users_data, users_file, indent=1)
    else:
        # AKO POSTOJI CITAMO GA RADI KASNIJEG KORISCENJA
        users_file = open(file_path, 'r')
        users_data = json.load(users_file)
        users_file.close()

    # ITERIRAMO KRZO KORISNIKE IZ GRUPE PRATECI NJIHOV INDEKS
    index: int = 0
    for member in group_members:
        # TVRAMO FAJL, CITAMO GA I ZATVARAMO
        with open(file_path, 'r') as file:
            data = json.load(file)

        # DOBIJAMO BROJ KORISNIKA IZ FAJLA
        users_l = len(data['users'])

        users_uids: List = []
        # AKO IMA KORISNIKA U FAJLU UPISUJEMO NJIHOVE ID-EVE KAKO IH NEBI DVA PUTA DODALI U ISTI FAJL
        if users_l > 0:
            users_uids = [usr['uid'] for usr in data['users']]

        # PROVERAVAMO DA LI SE ID TRENUTNOG KORISNIKA NALAZI UNUTRA FAJLA
        if member.id not in users_uids:
            # AKO SE NE NALAZI KREIRAMO OBJEKTA SA PODACIMA KORISNIKA
            new_user = {
                "uid": int(member.id),
                "access_hash": member.access_hash,
            }
            # OTVARAMO FAJL, I U NJEGA UPISUJEMO OBJEKAT
            f = open(file_path, "w")
            data['users'].append(new_user)
            json.dump(data, f, indent=1)
            f.close()
            index += 1


async def scrape_group(link, file_name):
    """
    POMOCU LINKA. DOBIJMAO OBJEKAT GRUPE IZ KOJE UZIMAMO KORISNIKE
    LINK DOBIJAMO OD KORISNIKA KAO I IME FAJLA U KOJI CE MO SKREJPOVATI KORISNIKE
    """

    # AUTROZACIJA AKUNTA I DOBIJANJE KLAJENT VARIJABLE
    client = await auth()

    # DOBIJAMO IME GRUPE IZ LINKA
    group_name = link.replace("https://t.me/", "")

    # DOBIJAMO OBJEKTA GRUPE
    group = await client.get_entity(group_name)

    group_members: List = []

    Utils.clear()

    # ITERIRAMO KROZ FILTERE
    # for index, f in enumerate(Utils.filters):

    try:
        # POKUSAVAMO DA UZMEMO KORISNIKE IZ GRUPE POMOCU FILTERA f
        participants = await client.get_participants(entity=group)
    except Exception as e:
        # AKO NE USPEMO ISPISUJEMO DATU GRESKU
        Utils.error("Participants" + str(e))
        pass

    group_members.extend(participants)

    # if participants:
    #     # AKO SMO U PRETHODNOM KORAKU DOBILI NEKE KORISNIKE ISPISUJEMO DA KORISTIMO FILTER
    #     Utils.warning(f"Using {index + 1}. filter: {f}")
    # else:
    #     # AKO NE SAMO NASTAVLJAMO NA SLEDECI FILTER
    #     continue

    # # PROVERAVAMO KOLICINU KORISNIKA PRE DODAVANJA NOVIH
    # old_len: int = len(group_members)

    # try:
    #     # ASINHRONO POKUSAVAMO DA DODAMO KORISNIKE KOJE SMO GORE DOBILE U grouo_members LISTE
    #     async for p in participants:
    #         group_members.append(p)
    # except Exception as e:
    #     # ISPISUJEMO DATU GRESKU
    #     Utils.error("Group Members" + str(e))
    #     pass

    # # PROVERAVAMO KOLICINU KORISNIKA POSLE DODAVANJ NOVIH
    # new_len: int = len(group_members)

    # # DOBIJAMO RAZLIKU POCETNE I DUZINE POSLED DODAVANJA RADI DEBAGOVANJE
    # count_of_newly_added = new_len - old_len
    # # ISPISUJEMO GORE NAVEDENU KOLICINU NOVO DODATIH KORISNIKA IZ TRENUTNOG FILTERA
    # Utils.debug(f"Added {count_of_newly_added} to group_members!")

    # POSLE ITERIRANJA KROZ SVE FILTERE ISPISUJEMO DA SMO USPESNO ZAVRSILI
    # KAO I BROJ KORISNIKA KOJE TREBA UPISATI U FAJL
    # Utils.ok("Used all filters!")
    print(len(group_members))

    # ASINHRONO CEKAMO FUNKCIJU KOJA UPISUJE KORISNIKE U ZADATI FAJL
    await write_users_to_file(group_members, file_name)
