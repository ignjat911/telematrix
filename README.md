## Name

Telematrix

## Description

Telegram Server Control Center - Marketing Tool

## API

API Setup
Go to http://my.telegram.org and log in.
Click on API development tools and fill the required fields.
put app name you want & select other in platform Example :
copy "api_id" & "api_hash" after clicking create app ( will be used in setup.py )

## Install python

• How To Install and Use
$ pkg install -y git python

## Clone

$ git clone https://gitlab.com/ignjat911/telematrix

$ cd telematrix

## Requirements and setup

Install requierments
$ python3 setup.py -i

## Data Scraping

1. Into a preexisting category

   $ python3 scraper.py -e

2. Into a newly created category

   $ python3 scraper.py -n

## Geolocational Scraping

1.  From an already saved location file:

        $ python3 geo.py -e

2.  From an newly created location file:

        $ python3 geo.py -n

## Adding Users

1.  Adding users from group to group
    (with source and destination links)

        $ python3 adder.py -gg

2.  Adding users from file to group
    (you can select a file from the files that you scraped before)

            $ python3 adder.py -gg

## Help

    $ python3 scraper.py --help
    $ python3 adder.py --help
    $ python3 geo.py --help
    $ python3 setup.py --help

## Update

Who to call?

## Roadmap

Plans

## Contributing

Contribution rules

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## License

Commercial

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

Track project status: https://docs.google.com/document/d/1cm4Rcj24birbkMlppnCDM-vW9fu7cKHw4Z4mfU6Udsg/edit?pli=1
